let table = document.querySelectorAll(".tbl");
let tblBody = document.querySelectorAll("tbody");
let tblHeader = document.querySelectorAll(".tbl-header");

for (let header = 0; header < tblHeader.length; header++) {
	tblHeader[header].addEventListener("click", sortData, false);
}

for (let tbody = 0; tbody < tblBody.length; tbody++) {
	if (tblBody[tbody])
		tblBody[tbody].addEventListener("click", highlightRow, false);
}

for (let tbl = 0; tbl < table.length; tbl++) {
	resizableGrid(table[tbl]);
}

function getCols(table) {
	let row = table.getElementsByTagName("tr")[0];
	let columnCount = row ? row.children : undefined;

	return columnCount;
}

function resizableGrid(table) {
	let cols = getCols(table);

	if (!cols)
		return;

	for (let i = 0; i < cols.length; i++) {
		let div = createDiv(table.offsetHeight);
		cols[i].appendChild(div);
		cols[i].style.position = "relative";
		setListeners(div);
	}

	function createDiv(height) {
		let div = document.createElement("div");
		div.style.top = 0;
		div.style.right = 0;
		div.style.width = "3px";
		div.style.position = "absolute";
		div.style.cursor = "col-resize";
		div.style.height = height + "px";
		div.className = "columnSelector";
		return div;
	}

	function setListeners(div) {
		let pageX, curCol, nxtCol, curColWidth, nxtColWidth;
		div.addEventListener("mousedown", function (e) {
			curCol = e.target.parentElement;
			nxtCol = curCol.nextElementSibling;
			pageX = e.pageX;
			curColWidth = curCol.offsetWidth;
			if (nxtCol)
				nxtColWidth = nxtCol.offsetWidth;
		});

		document.addEventListener("mousemove", function (e) {
			if (curCol) {
				let diffX = e.pageX - pageX;

				if (nxtCol)
					nxtCol.style.width = (nxtColWidth - (diffX)) + "px";

				curCol.style.width = (curColWidth + diffX) + "px";
			}
		});

		document.addEventListener("mouseup", function (e) {
			curCol = undefined;
			nxtCol = undefined;
			pageX = undefined;
			nxtColWidth = undefined;
			curColWidth = undefined;
		});
	}
}

function sortData() {
	alert("Add sorting code");
}

function highlightRow(e) {
	let clickedElement = e.target;

	if (clickedElement.nodeName.toLowerCase() == "td") {
		if (clickedElement.parentElement.style.color == "white" &&
			clickedElement.parentElement.bgColor == "#2A628F") {
			clickedElement.parentElement.style.color = "";
			clickedElement.parentElement.bgColor = "";
		}
		else {
			clickedElement.parentElement.style.color = "white";
			clickedElement.parentElement.bgColor = "#2A628F";
		}
	}
}

function openTab(e, tabName) {
	let i, tabContent, tabLinks;
	tabContent = document.getElementsByClassName("tab-content");
	for (i = 0; i < tabContent.length; i++) {
		tabContent[i].style.display = "none";
	}
	tabLinks = document.getElementsByClassName("tab-links");
	for (i = 0; i < tabLinks.length; i++) {
		tabLinks[i].className = tabLinks[i].className.replace(" active", "");
	}
	document.getElementById(tabName).style.display = "block";
	e.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();


let transactionFld = document.querySelectorAll(".total-transaction-field");
for (let count = 0; count < transactionFld.length; count++) {
	transactionFld[count].readOnly = true;
}


function showModal(button) {
	let modal = document.querySelector("#newModal");
	let modalTitle = document.querySelector(".modal-title");
	let form = document.querySelector(".new-modal-form");
	let newBtn = button.getAttribute("data-name");
	let label = document.querySelector("#firstFieldLbl");

	while (modalTitle.firstChild) {
		modalTitle.removeChild(modalTitle.firstChild);
	}

	if (newBtn === "newSupplier") {
		let title = document.createTextNode("New Supplier");
		label.nextElementSibling.id = "supplierName";
		label.nextElementSibling.name = "supplierName";
		modalTitle.appendChild(title);
		form.action = "addSupplier";
		label.innerHTML = "Supplier: ";
	}
	else if (newBtn === "newCustomer") {
		let title = document.createTextNode("New Customer");
		label.nextElementSibling.id = "customerName";
		label.nextElementSibling.name = "customerName";
		modalTitle.appendChild(title);
		form.action = "addCustomer";
		label.innerHTML = "Customer: ";
	}

	modal.style.display = "block";
}

function closeModal() {
	let modal = document.querySelector("#newModal");
	modal.style.display = "none";
}

window.onclick = function (event) {
	let modal = document.querySelector("#newModal");
	if (event.target == modal) {
		modal.style.display = "none";
	}
}