const tbl = document.querySelectorAll(".tbl");
const tblBody = document.querySelectorAll("tbody");
const tblHdr = document.querySelectorAll(".tbl-header");

for (let hdr = 0; hdr < tblHdr.length; hdr++) {
	tblHdr[hdr].addEventListener("click", sortData, false);
}
for (let tbody = 0; tbody < tblBody.length; tbody++) {
	if (tblBody[tbody]) {
		tblBody[tbody].addEventListener("click", highlightRow, false);
	}
}
for (let tbl = 0; tbl < tbl.length; tbl++) {
	resizableGrid(tbl[tbl]);
}

function getCols(tbl) {
	const row = tbl.getElementsByTagName("tr")[0];
	const colCnt = row ? row.children : undefined;
	return colCnt;
}

function resizableGrid(tbl) {
	const cols = getCols();

	if (!cols) { return; }
	else {
		for (let i = 0; i < (cols.length - 1); i++) {
			const div = createDiv(tbl.offsetHeight);
			cols[i].appendChild(div);
			cols[i].style.position = "relative";
			setListeners(div);
		}

		function createDiv(height) {
			const div = document.createElement("div");
			div.style.top = 0;
			div.style.right = 0;
			div.style.width = "3px";
			div.style.position = "absolute";
			div.style.cursor = "col-resize";
			div.style.height = height + "px";
			div.className = "columnSelector";
			return div;
		}

		function setListeners(div) {
			let pageX, curCol, nxtCol, curColWidth, nxtColWidth;
			div.addEventListener("mousedown", function (e) {
				curCol = e.target.parentElement;
				nxtCol = curCol.nextElementSibling;
				pageX = e.pageX;
				curColWidth = curCol.offsetWidth;

				if (nxtCol) { nxtColWidth = nxtCol.offsetWidth; }
			});

			document.addEventListener("mousemove", function (e) {
				if (curCol) {
					const diffX = e.pageX - pageX;

					if (nxtCol) {
						nxtCol.style.width = (nxtColWidth - (diffX)) + "px";
					}
					curCol.style.width = (curColWidth + diffX) + "px";
				}
			});

			document.addEventListener("mouseup", function (e) {
				curCol = undefined;
				nxtCol = undefined;
				pageX = undefined;
				nxtColWidth = undefined;
				curColWidth = undefined;
			});
		}
	}
}

function sortData() {
	alert("Add sorting code");
}

function highlightRow(e) {
	let clickedElement = e.target;

	if (clickedElement.nodeName.toLowerCase() == "td") {
		if (clickedElement.parentElement.style.color == "white" &&
			clickedElement.parentElement.bgColor == "#2A628F") {
			clickedElement.parentElement.style.color = "";
			clickedElement.parentElement.bgColor = "";
		}
		else {
			clickedElement.parentElement.style.color = "white";
			clickedElement.parentElement.bgColor = "#2A628F";
		}
	}
}

function openTab(e, tabName) {
	let i, tabContent, tabLinks;
	tabContent = document.getElementsByClassName("tab-content");
	for (i = 0; i < tabContent.length; i++) {
		tabContent[i].style.display = "none";
	}
	tabLinks = document.getElementsByClassName("tab-links");
	for (i = 0; i < tabLinks.length; i++) {
		tabLinks[i].className = tabLinks[i].className.replace(" active", "");
	}
	document.getElementById(tabName).style.display = "block";
	e.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();


let transactionFld = document.querySelectorAll(".total-transaction-field");
for (let count = 0; count < transactionFld.length; count++) {
	transactionFld[count].readOnly = true;
}


function showModal(button) {
	let modal = document.querySelector("#newModal");
	let modalTitle = document.querySelector(".modal-title");
	let form = document.querySelector(".new-modal-form");
	let newBtn = button.getAttribute("data-name");
	let label = document.querySelector("#firstFieldLbl");

	while (modalTitle.firstChild) {
		modalTitle.removeChild(modalTitle.firstChild);
	}

	if (newBtn === "newSupplier") {
		let title = document.createTextNode("New Supplier");
		label.nextElementSibling.id = "supplierName";
		// label.nextElementSibling.name = "name";
		modalTitle.appendChild(title);
		form.action = "suppliers";
		label.innerHTML = "Supplier: ";
	}
	else if (newBtn === "newCustomer") {
		let title = document.createTextNode("New Customer");
		label.nextElementSibling.id = "customerName";
		// label.nextElementSibling.name = "name";
		modalTitle.appendChild(title);
		form.action = "customers";
		label.innerHTML = "Customer: ";
	}

	modal.style.display = "block";
}

function checkFields() {
	// e.preventDefault();
	let addBtn = document.querySelector(".add-btn");
	let name = document.querySelector(".new-modal-form .name").value;
	let address = document.querySelector(".new-modal-form #address").value;
	let contact = document.querySelector(".new-modal-form #mobile_no").value;
	let form = document.querySelector(".new-modal-form");
	if (name.trim() == '' || address.trim() == '' || contact.trim() == '') {
		alert("All fields should be filled out");
		return;
	}
	if (/\D/.test(contact)) {
		alert("Contact number should only contain numbers");
		return;
	}
	postRequest(form.action, new FormData(form), (responseText) => {
		let response = JSON.parse(responseText);
		if(response.success) {
			alert(response.message);
		}
		else {
			alert("Error occured");
		}
		closeModal();
	});
	// let xhr = new XMLHttpRequest();
	// xhr.onreadystatechange = () => {
	// 	if(xhr.readyState == 4) {
	// 		let response = JSON.parse(xhr.responseText);
	// 		if(response.success) {
	// 			alert(response.message);
	// 		}
	// 		else {
	// 			alert("Error occured");
	// 		}
	// 		closeModal();
	// 	}
	// };

	// xhr.open(form.method, form.action, true);
	// xhr.send(new FormData(form));
}

function closeModal() {
	let modal = document.querySelector("#newModal");
	modal.style.display = "none";
}

window.onclick = function (event) {
	let modal = document.querySelector("#newModal");
	if (event.target == modal) {
		modal.style.display = "none";
	}
}

// let createSupplier = () => {
// 	postRequest(path, data, callback);
// 	let xhr = new XMLHttpRequest();
// 	xhr.onreadystatechange = () => {
// 		if(xhr.readyState == 4) {
// 			alert(xhr.responseText);
// 		}
// 	};

// 	xhr.open('POST', "/uploadFile", true);
// 	let data = new FormData;
// 	data.append('file', file);
// 	xhr.send(new FormData(document.querySelector("#form-supplier")));
// };

let addIncomingOrder = () => {
	let form = document.querySelector(".form");
	let form_original = new FormData(form);
	let form_send = new FormData();
	form_send.append("supplier_id", form_original.get("supplier_id"));
	form_send.append("item_id", form_original.get("item"));
	form_send.append("quantity", form_original.get("quantity"));
	form_send.append("delivery_date", form_original.get("date-incoming"));
	console.log(form_send.get("supplier_id"));
	postRequest("incoming-orders", form_send, (responseText) => {
		let response = JSON.parse(responseText);
		if(response.success) {
			alert(response.message);
		}
		else {
			alert("Error occured");
		}
	});
}

let item_query = {
	"stock_num": null,
	"brand": null,
	"description": null,
	"name": null
};

let item_id = null;

//[[name, value], [key, value]]
function autocomplete(inp, input_id, path, searchKey, query_object, click_callback) {
	/*the autocomplete function takes two arguments,
	the text field element and an array of possible autocompleted values:*/
	let currentFocus;
	/*execute a function when someone writes in the text field:*/
	inp.addEventListener("input", function (e) {
		if(input_id != null) {
			input_id.value = "";
		}
		let autocompleteDiv, matchDiv, input = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();
		if (!input) { return false; }
		currentFocus = -1;
		/*create a DIV element that will contain the items (values):*/
		autocompleteDiv = document.createElement("DIV");
		autocompleteDiv.setAttribute("id", this.id + "autocomplete-item");
		autocompleteDiv.setAttribute("class", "autocomplete-items");
		/*append the DIV element as a child of the autocomplete container:*/
		this.parentNode.appendChild(autocompleteDiv);

		query_object[searchKey] = input;
		let params = {};
		
		for(let [key, value] of Object.entries(query_object)) {
			if(value != null) {
				params[key] = value;
			}
		}
		console.log(params);
		let searchParams = new URLSearchParams(params);
		getRequest(path + "?" + searchParams.toString(), (res) => {
			let results = res.result;
			for (let result of results) {
				/*check if the item starts with the same letters as the text field value:*/
				/*create a DIV element for each matching element:*/
				matchDiv = document.createElement("DIV");
				/*make the matching letters bold:*/
				let match_input = (result[searchKey]).match(new RegExp(input, "i"))
				matchDiv.innerHTML = (result[searchKey]).replace(new RegExp(input, "i"), "<strong>" + match_input + "</strong>");
				/*insert a input field that will hold the current array item's value:*/
				if(input_id != null) {
					matchDiv.innerHTML += "<input type='hidden' class='id' value='" + result.id + "'>";
				}
				matchDiv.innerHTML += "<input type='hidden' class='name' value='" + result[searchKey] + "'>";
				/*execute a function when someone clicks on the item value (DIV element):*/
				matchDiv.addEventListener("click", function (e) {
					/*insert the value for the autocomplete text field:*/
					inp.value = this.querySelectorAll("input.name")[0].value;
					if(input_id != null) {
						input_id.value = this.querySelectorAll("input.id")[0].value;
					}
					query_object[searchKey] = inp.value;
					if(click_callback != null) {
						click_callback();
					}
					closeAllLists();
				});
				autocompleteDiv.appendChild(matchDiv);
			}
		});
	});

	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener("keydown", function (e) {
		let matchItems = document.getElementById(this.id + "autocomplete-item");
		if (matchItems) matchItems = matchItems.querySelectorAll("div");
		if (e.keyCode == 40) {
			/*If the arrow DOWN key is pressed,
			increase the currentFocus variable:*/
			currentFocus++;
			/*and and make the current item more visible:*/
			addActive(matchItems);
		} else if (e.keyCode == 38) { //up
			/*If the arrow UP key is pressed,
			decrease the currentFocus variable:*/
			currentFocus--;
			/*and and make the current item more visible:*/
			addActive(matchItems);
		} else if (e.keyCode == 13) {
			/*If the ENTER key is pressed, prevent the form from being submitted,*/
			e.preventDefault();
			if (currentFocus > -1) {
				/*and simulate a click on the "active" item:*/
				if (matchItems) matchItems[currentFocus].click();
			}
		}
	});
	function addActive(items) {
		/*a function to classify an item as "active":*/
		if (!items) return false;
		/*start by removing the "active" class on all items:*/
		removeActive(items);
		if (currentFocus >= items.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (items.length - 1);
		/*add class "autocomplete-active":*/
		items[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(items) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (let index = 0; index < items.length; index++) {
			items[index].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/
		let items = document.getElementsByClassName("autocomplete-items");
		for (let index = 0; index < items.length; index++) {
			if (elmnt != items[index] && elmnt != inp) {
				items[index].parentNode.removeChild(items[index]);
			}
		}
	}
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);
	});
}

let itemSearchCallback = () => {
	let item = {
		stock_num : {
			value: null,
			id: "stock-no"
		},
		name : {
			value: null,
			id: "item"
		},
		brand : {
			value: null,
			id: "brand"
		},
		unit_price : {
			value: null,
			id: "unit-price"
		},
		unit : {
			value: null,
			id: "unit"
		},
		description : {
			value: null,
			id: "description"
		},
		selling_price : {
			value: null,
			id: "selling-price"
		}
	};

	let searchParams = new URLSearchParams();
	for(let [key, value] of Object.entries(item_query)) {
		if(value != null) {
			searchParams.append(key, value);
		}
	}
	console.log(searchParams);
	
	getRequest("items?" + searchParams.toString(), (res) => {
		if(res.result.length == 1) {
			let result = res.result[0];
			for(let key in item) {
				console.log(key)
				if(key == "id") {
					item_id = result[key];
				}
				else {
					item[key].value = result[key];
					console.log(item[key].id);
					document.querySelector(`#${item[key].id}`).value = result[key];
				}
			}
		}
	});
};

/*An array containing all the country names in the world:*/
let countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(
	document.querySelector("#supplier"),
	document.querySelector("#supplier_id"),
	"suppliers/search",
	"name",
	{"name": null}
);

autocomplete(
	document.querySelector("#stock-no"),
	null,
	"items/stock_num",
	"stock_num",
	item_query,
	itemSearchCallback
);

autocomplete(
	document.querySelector("#brand"),
	null,
	"items/brand",
	"brand",
	item_query,
	itemSearchCallback
);

autocomplete(
	document.querySelector("#item"),
	null,
	"items/name",
	"name",
	item_query,
	itemSearchCallback
);

autocomplete(
	document.querySelector("#description"),
	null,
	"items/description",
	"description",
	item_query,
	itemSearchCallback
);
