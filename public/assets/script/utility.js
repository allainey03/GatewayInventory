let ip = "http://localhost:8080";

let getRequest = (path, callback) => {
	let rq = new XMLHttpRequest();
	rq.open("GET", ip + "/" + path, true);
	rq.onreadystatechange = () => {
		if(rq.readyState == 4) {
			callback(JSON.parse(rq.responseText));
		}
	};
	rq.send();
};

let postRequest = (path, data, callback) => {
	let rq = new XMLHttpRequest();
	rq.open("POST", ip + "/" + path, true);
	rq.onreadystatechange = () => {
		if(rq.readyState == 4) {
			callback(rq.responseText);
		}
	};
	if(data instanceof FormData) {
		rq.send(data);
	}
	else {
		rq.setRequestHeader("Content-Type", "application/json");
		rq.send(JSON.stringify(data));
	}
};