const tbl = document.querySelector(".tbl");
const tblBody = document.querySelector(".tbl-body");
const tblHdr = document.querySelectorAll(".tbl-header");
const page = document.querySelector(".show-page");
let select = document.querySelector(".show-page");

for (let hdr = 0; hdr < tblHdr.length; hdr++) {
	tblHdr[hdr].addEventListener("click", sortData, false);
}
tblBody.addEventListener("click", highlightRow, false);
select.addEventListener("change", createTblBody, false);

createTblBody();
resizableGrid(tbl);

function getCols() {
	const row = tbl.getElementsByTagName("tr")[0];
	const colCnt = row ? row.children : undefined;
	return colCnt;
}

function createTblBody() {
	const cols = getCols();

	if (!cols) { return; }
	else {
		const showRows = parseInt(page.value);
		let tblChild = tblBody.lastElementChild;
		let rows = 10;
		let maxRows = 0;

		while (tblChild) {
			tblBody.removeChild(tblChild);
			tblChild = tblBody.lastElementChild;
		}

		if (rows < showRows)
			maxRows = rows;
		else
			maxRows = showRows;

		for (let rowCount = 0; rowCount < maxRows; rowCount++) {
			const tblRow = document.createElement("tr");
			tblRow.id = rowCount;
			tblRow.className = "tbl-row";
			tblBody.appendChild(tblRow);

			for (let colCount = 0; colCount < cols.length; colCount++) {
				let tblData = document.createElement("td");
				let content = "yaw";
				tblData.className = "tbl-data";
				tblRow.appendChild(tblData);
				tblData.innerHTML = content;
			}
		}
	}
}

function resizableGrid(tbl) {
	const cols = getCols();

	if (!cols) { return; }
	else {
		for (let i = 0; i < (cols.length - 1); i++) {
			const div = createDiv(tbl.offsetHeight);
			cols[i].appendChild(div);
			cols[i].style.position = "relative";
			setListeners(div);
		}

		function createDiv(height) {
			const div = document.createElement("div");
			div.style.top = 0;
			div.style.right = 0;
			div.style.width = "3px";
			div.style.position = "absolute";
			div.style.cursor = "col-resize";
			div.style.height = height + "px";
			div.className = "columnSelector";
			return div;
		}

		function setListeners(div) {
			let pageX, curCol, nxtCol, curColWidth, nxtColWidth;
			div.addEventListener("mousedown", function (e) {
				curCol = e.target.parentElement;
				nxtCol = curCol.nextElementSibling;
				pageX = e.pageX;
				curColWidth = curCol.offsetWidth;

				if (nxtCol) { nxtColWidth = nxtCol.offsetWidth; }
			});

			document.addEventListener("mousemove", function (e) {
				if (curCol) {
					const diffX = e.pageX - pageX;

					if (nxtCol) {
						nxtCol.style.width = (nxtColWidth - (diffX)) + "px";
					}
					curCol.style.width = (curColWidth + diffX) + "px";
				}
			});

			document.addEventListener("mouseup", function (e) {
				curCol = undefined;
				nxtCol = undefined;
				pageX = undefined;
				nxtColWidth = undefined;
				curColWidth = undefined;
			});
		}
	}
}

function sortData() {
	alert("Add sorting code");
}

let prevHighlight = null;
function highlightRow(e) {
	let clickedElement = e.target;
	let tblRow = document.querySelectorAll(".tbl-row");

	if (clickedElement.nodeName.toLowerCase() == "td") {
		if (clickedElement.parentElement.style.color == "white" &&
			clickedElement.parentElement.bgColor == "#2A628F") {
			clickedElement.parentElement.style.color = "";
			clickedElement.parentElement.bgColor = "";
			prevHighlight = null;
		}
		else {
			clickedElement.parentElement.style.color = "white";
			clickedElement.parentElement.bgColor = "#2A628F";

			if (prevHighlight !== null) {
				tblRow[prevHighlight].style.color = "";
				tblRow[prevHighlight].bgColor = "";
			}
			prevHighlight = parseInt(clickedElement.parentElement.id);
		}
	}
}

function changeHeader(select) {
	let firstLabel = document.querySelector("#firstLabel");
	let firstInput = document.querySelector(".inputField");
	let from = document.querySelector("#from");
	let to = document.querySelector("#to");
	let header = document.querySelector("#header2");
	header.parentElement.style.display = "inline";
	from.value = null;
	to.value = null;

	if (select.value === "outgoing") {
		firstLabel.style.display = "none";
		firstInput.style.display = "none";
		header.innerHTML = "Customer";
	}
	else if (select.value === "incoming") {
		firstLabel.style.display = "none";
		firstInput.style.display = "none";
		header.innerHTML = "Supplier";
	}
	else if (select.value === "supplier") {
		firstLabel.style.display = "inline";
		firstInput.style.display = "inline";
		firstLabel.innerHTML = "Supplier:"
		firstInput.id = "supplier";
		header.parentElement.style.display = "none";
	}
	else {
		firstLabel.style.display = "inline";
		firstInput.style.display = "inline";
		firstLabel.innerHTML = "Customer:"
		firstInput.id = "customer";
		header.parentElement.style.display = "none";
	}
}