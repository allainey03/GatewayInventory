-- set global log_bin_trust_function_creators=1;

DROP TABLE IF EXISTS `incoming_orders`;
DROP TABLE IF EXISTS `inventory`;
DROP TABLE IF EXISTS `outgoing_orders`;
DROP TABLE IF EXISTS `customers`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `items`;
DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(50) NOT NULL UNIQUE,
	`password` varchar(255) NOT NULL,
	`first_name` varchar(50) NOT NULL,
	`last_name` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
);

-- INSERT INTO users (username, password, first_name, last_name) VALUES ('allainey', '12345', 'Allaine', 'Bautista');
-- INSERT INTO users (username, password, first_name, last_name) VALUES ('pauly', '55555', 'Paul', 'Bajo');

CREATE TABLE `items` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`stock_num` varchar(255) NOT NULL,
	`brand` varchar(255) NOT NULL,
	`description` varchar(255),
	`type` varchar(255) NOT NULL,
	`unit` varchar(255) NOT NULL,
	`selling_price` int(11) NOT NULL,
	PRIMARY KEY (`id`)
);

INSERT INTO items (stock_num, brand, type, description, selling_price, unit) VALUES ('UX331UN', 'ASUS', 'laptop', '13in 4GB RAM', 50000, 'box');
INSERT INTO items (stock_num, brand, type, description, selling_price, unit) VALUES ('Rechargeable fan', 'Tough Mama', 'fan', 'stand fan', 2500, 'box');
INSERT INTO items (stock_num, brand, type, description, selling_price, unit) VALUES ('4K TV','Samsung', 'television', '32in Smart TV', 25000, 'box');
INSERT INTO items (stock_num, brand, type, description, selling_price, unit) VALUES ('16GB flash drive', 'Lexar', 'flash drive', 'flash drive', 1000, 'package');

CREATE TABLE `suppliers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`address` varchar(255) NOT NULL,
	`mobile_no` bigint(20) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `customers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`address` varchar(255) NOT NULL,
	`mobile_no` bigint(20) NOT NULL,
	PRIMARY KEY (`id`)
);

-- INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier1', 'Bajada, Davao City', 09114455667);
-- INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier2', 'Makati City', 09874569123);
-- INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier3', 'Mati City', 09789456123);
-- INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier4', 'Buhangin, Davao City', 09332156447);

CREATE TABLE `incoming_orders` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`supplier_id` int(11) NOT NULL,
	`item_id` int(11) NOT NULL,
	`unit_price` int(11) NOT NULL,
	`quantity` int(11) NOT NULL,
	`delivery_date` date NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`),
	FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
);

-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (1, 2, 10, 250000, '2017-01-05');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (4, 1, 5, 6400000, '2017-12-10');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (3, 4, 20, 120000, '2018-05-17');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (2, 3, 8, 2700000, '2018-07-22');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (1, 1, 10, 250000, '2017-09-05');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (2, 2, 12, 6400000, '2017-10-10');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (3, 3, 8, 120000, '2018-08-17');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (4, 4, 15, 2700000, '2018-01-22');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (1, 3, 10, 250000, '2018-02-05');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (2, 4, 12, 6400000, '2018-11-10');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (3, 1, 8, 120000, '2018-06-17');
-- INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (4, 2, 15, 2700000, '2018-02-22');

CREATE TABLE `inventory` (
	`item_id` int(11) NOT NULL UNIQUE,
	`quantity` int(11) NOT NULL,
	FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
);

-- INSERT INTO inventory (item_id, quantity, selling_price) VALUES (1, 23, 6000000);
-- INSERT INTO inventory (item_id, quantity, selling_price) VALUES (2, 37, 300000);
-- INSERT INTO inventory (item_id, quantity, selling_price) VALUES (3, 26, 3000000);
-- INSERT INTO inventory (item_id, quantity, selling_price) VALUES (4, 47, 150000);

CREATE TABLE `outgoing_orders` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`item_id` int(11) NOT NULL,
	`sold_price` int(11) NOT NULL,
	`quantity` int(11) NOT NULL,
	`transaction_date` date NOT NULL,
	`customer_id` int(11) NOT NULL,
	`employee_id` int(11) NOT NULL,
	FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
	FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
	FOREIGN KEY (`employee_id`) REFERENCES `users` (`id`),
	PRIMARY KEY(`id`)
);

-- CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `deliveries_view` AS select deliveries.id AS id, suppliers.name AS supplier, items_base.stock_num AS item, items_base.brand AS brand, items.description AS description, deliveries.quantity AS quantity, deliveries.buying_price AS buying_price, deliveries.delivery_date AS delivery_date from ((deliveries left join (items left join items_base on (items.item_base_id = items_base.id)) on (deliveries.item_id = items.id)) left join suppliers on(deliveries.supplier_id = suppliers.id));

CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `inventory_view` AS select inventory.item_id AS id, items.stock_num AS item, items.brand AS brand, items.description AS description, items.unit AS unit, inventory.quantity AS quantity, items.selling_price AS selling_price from inventory join items on inventory.item_id = items.id;

-- CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `transactions_view` AS select transactions.id AS id, items_base.stock_num AS item, items_base.brand AS brand, transactions.quantity AS quantity, transactions.price_sold AS price_sold, transactions.transaction_date AS transaction_date, concat(users.first_name, ' ', users.last_name) AS employee from ((transactions left join (items left join items_base on (items.item_base_id = items_base.id)) on((transactions.item_id = items.id))) left join users on((transactions.employee = users.username)));

-- CREATE TRIGGER before_incoming_update
-- 	BEFORE INSERT ON incoming_orders
-- 	FOR EACH ROW
-- 		INSERT INTO inventory 
-- 		SET item_id = NEW.item_id,
-- 			quantity = NEW.quantity
-- 		ON DUPLICATE KEY UPDATE quantity = quantity + NEW.quantity;

CREATE OR REPLACE VIEW test_inventory AS
SELECT i.id AS item, COALESCE(SUM(io.quantity), 0) - COALESCE(SUM(oo.quantity), 0) AS quantity
FROM items i
LEFT JOIN incoming_orders io ON i.id = io.item_id
LEFT JOIN outgoing_orders oo ON i.id = oo.item_id
GROUP BY i.id
