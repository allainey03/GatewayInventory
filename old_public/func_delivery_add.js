let changedSupplierType = () => {
	let select = document.getElementById("select_supplier_type");
	let type = select.value;
	if(type == "id") {
		document.getElementById("select_supplier").remove();
		let input_supplier_id = document.createElement("input");
		input_supplier_id.type = "text";
		input_supplier_id.id = "input_supplier_id";
		select.after(input_supplier_id);
	}
	else if(type == "name") {
		document.getElementById("input_supplier_id").remove();
		let select_supplier = document.createElement("select");
		select_supplier.id = "select_supplier";
		getRequest("supplier/all", (rslt) => {
			console.log(rslt);
			let rslt_json = JSON.parse(rslt);
			if(rslt_json.result) {
				for(let entry of rslt_json.result) {
					let new_option = document.createElement("option");
					new_option.text = entry.name;
					new_option.value = entry.id;
					select_supplier.add(new_option);
				}
				select.after(select_supplier);
			}
			else {
				console.log("Error");
			}
		});
	}
};

let updateSupplierDetails = () => {
	let supplier_id = document.getElementById("supplier_id");
	let supplier_name = document.getElementById("supplier_name");
	let supplier_address = document.getElementById("supplier_address");
	let id = document.getElementById("input_supplier_id").value;
	getRequest("supplier/id/" + id, (rslt) => {
		console.log(rslt);
		let rslt_json = JSON.parse(rslt);
		if(rslt_json.result) {
			let supplier = rslt_json.result[0];
			supplier_id.innerHTML = supplier.id;
			supplier_name.innerHTML = supplier.name;
			supplier_address.innerHTML = supplier.address;
		}
		else {
			console.log("Error");
		}
	});
};

let changedItemType = () => {
	console.log("Changed Item");
	let select = document.getElementById("select_item_type");
	let type = select.value;
	if(type == "id") {
		document.getElementById("select_item").remove();
		let input_item_id = document.createElement("input");
		input_item_id.type = "text";
		input_item_id.id = "input_item_id";
		select.after(input_item_id);
	}
	else if(type == "stock_num") {
		document.getElementById("input_item_id").remove();
		let select_item = document.createElement("select");
		select_item.id = "select_item";
		select_item.onchange = updateVariants;
		getRequest("items_base/all", (rslt) => {
			console.log(rslt);
			let rslt_json = JSON.parse(rslt);
			if(rslt_json.result) {
				for(let entry of rslt_json.result) {
					let new_option = document.createElement("option");
					new_option.text = entry.brand + " - " + entry.stock_num;
					new_option.value = entry.id;
					select_item.add(new_option);
				}
				select.after(select_item);
				updateVariants();
			}
			else {
				console.log("Error");
			}
		});
	}
};

let updateVariants = () => {
	let select_variant = document.getElementById("select_variant");
	let item_id = document.getElementById("select_item") ? document.getElementById("select_item").value : document.getElementById("input_item_id").value;
	for(let i = 0; i < select_variant.length; i++) {
		select_variant.remove(i);
	}
	getRequest("item?item_base_id=" + item_id, (rslt) => {
		console.log(rslt);
		let rslt_json = JSON.parse(rslt);
		if(rslt_json.result) {
			for(let entry of rslt_json.result) {
				let new_option = document.createElement("option");
				new_option.text = entry.description;
				select_variant.add(new_option);
			}
		}
		else {
			console.log("Error");
		}
	});
};

let addDeliverySubmit = () => {
	console.log("submit");
	let supplier = document.getElementById("select_supplier")? document.getElementById("select_supplier").value: document.getElementById("input_supplier_id").value;
	let item = document.getElementById("select_item")? document.getElementById("select_item").value: document.getElementById("input_item_id").value;
	let quantity = document.getElementById("inputQuantity").value;
	let buying_price = document.getElementById("inputBuyingPrice").value;
	let delivery_date = document.getElementById("inputDeliveryDate").value;
	if(supplier == "" || item == "" || quantity == "" || buying_price == "" || delivery_date == "") {
		alert("Please fill out all the fields");
	}
	else {
		let obj = {
			supplier_id: supplier,
			item_id: item,
			quantity: quantity,
			buying_price: buying_price,
			delivery_date: delivery_date
		};
		console.log(obj);
		postRequest("delivery", obj, (response) => {
			console.log(response);
			let obj_resp = JSON.parse(response);
			if(obj_resp.success) {
				document.getElementById("cover").className = "hidden";
				alert(obj_resp.success);
				loadDeliveriesTable();
			}
		});
	}
};
