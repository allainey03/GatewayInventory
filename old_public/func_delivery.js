window.onload = () => {
	checkSession((res) => {
		if(res) {
			loadDeliveriesTable();
		}
		else {
			window.location.href = "/";
		}
	});
};

let loadDeliveriesTable = () => {
	getRequest("delivery/all", (rslt) => {
		console.log(rslt);
		let rslt_json = JSON.parse(rslt);
		let deliveries_table = document.getElementById("deliveries_table");
		if(rslt_json.result) {
			populateTable(deliveries_table, ["id","supplier","item","brand","description","quantity","buying_price","delivery_date"], rslt_json.result);
		}
		else {
			console.log("Error");
		}
	});
};

let searchDeliveries = () => {
	let type = document.getElementById("select_deliveries_type").value;
	let search = document.getElementById("deliveries_search").value;
	let path = "delivery/search?" + type + "=" + search;
	if(search == "") {
		path = "delivery/all";
	}
	getRequest(path, (rslt) => {
		console.log(rslt);
		let rslt_json = JSON.parse(rslt);
		let deliveries_table = document.getElementById("deliveries_table");
		if(rslt_json.result) {
			populateTable(deliveries_table, ["id","supplier","item","brand","description","quantity","buying_price","delivery_date"], rslt_json.result);
		}
		else {
			console.log("Error");
		}
	});
};

let addDeliveryClicked = () => {
	window.location.href = "/deliveries/add";
	/* document.getElementById("cover").classList.remove("hidden");
	document.getElementById("content_addDelivery").className = "cover_content"; */
	/* let body = document.getElementById("body");
	body.innerHTML = "<div>" +
			"<div class=\"center\">" +
				"<h3>Add delivery</h3>" +
			"</div>" +
			"<div>" +
				"<label>Supplier:</label>" +
				"<select id=\"select_supplier_type\" onchange=\"changedSupplierType()\">" +
					"<option value=\"id\" selected>ID</option>" +
					"<option value=\"name\">Name</option>" +
				"</select>" +
				"<input type=\"text\" id=\"input_supplier_id\"/>" +
				"<!-- <span class=\"error_message\" id=\"error_quantity\"></span> -->" +
			"</div>" +
			"<div>" +
				"<label>Item: </label>" +
				"<select id=\"select_item_type\" onchange=\"changedItemType()\">" +
					"<option value=\"id\" selected>ID</option>" +
					"<option value=\"stock_num\">Stock Number</option>" +
				"</select>" +
				"<input type=\"text\" id=\"input_item_id\" onblur=\"updateVariants()\"/>" +
				"<!-- <option value=\"\" disabled selected>Select an item</option>> -->" +
				"<!-- <span class=\"error_message\" id=\"error_quantity\"></span> -->" +
			"</div>" +
			"<div> Variant: <select id=\"select_variant\"></select></div>" +
			"<div>" +
				"<label>Quantity: </label><input type=\"text\" id=\"inputQuantity\"/><span class=\"error_message\" id=\"error_quantity\"></span>" +
			"</div>" +
			"<div>" +
				"<label>Buying Price: </label><input type=\"text\" id=\"inputBuyingPrice\"/><span class=\"error_message\" id=\"error_buyingPrice\"></span>" +
			"</div>" +
			"<div>" +
				"<label>Date: </label><input type=\"date\" id=\"inputDeliveryDate\"/>" +
			"</div>" +
			"<div class=\"center\">" +
				"<div class=\"error_message\" id=\"error_submit\"></div>" +
				"<button onclick=\"addDeliverySubmit()\">Submit</button><button onclick=\"cancelClicked('cover')\">Cancel</button>" +
			"</div>" +
		"</div>" */
};
