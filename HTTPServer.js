const http = require("http");
const StaticServer = require("node-static").Server;
const file = new StaticServer('./public');
let mysql = require('mysql');
let qs = require('querystring');
const formidable = require('formidable');
const conn = mysql.createConnection({ host: "localhost", user : "testdb", password : "testdb", database : "testdb" });
let handlers = {};
conn.connect();

// handlers["/signin"] = (req, res) => {
// 	if(req.method == "POST") {
// 		extractData(req, (data) => {
// 			let account = JSON.parse(data);
// 			findFromDB("users", account, (rslt) => {
// 				res.writeHead(200, {'Content-Type' : 'application/json'});
// 				res.end(JSON.stringify(rslt));
// 			});
// 		});
// 	}
// };

handlers["/"] = (req, res) => {
	if(!req.url) {
		file.serveFile("/inventory.html", 200, {}, req, res);
	}
	else {
		understandRemainingPath(res, req.url, "inventory_view");
	}
};

handlers["/transactions"] = (req, res) => {
	if(!req.url) {
		file.serveFile("/pages/transactions.html", 200, {}, req, res);
	}
	// else {
	// 	understandRemainingPath(res, req.url, "inventory_view");
	// }
};

handlers["/reports"] = (req, res) => {
	if(!req.url) {
		file.serveFile("/pages/reports.html", 200, {}, req, res);
	}
	else {
		understandRemainingPath(res, req.url, "inventory_view");
	}
};

handlers["/inventory"] = (req, res) => {
	if(req.method == "GET") {
		understandRemainingPath(res, req.url, "inventory_view");
	}
}

handlers["/incoming-orders"] = (req, res) => {
	if(req.method == "POST") {
		let form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files) {
			insertIntoDB("incoming_orders", fields, (result) => {
				if(result.result) {
					res.end(JSON.stringify({
						success: true,
						message: "Order was successfully added."
					}));
				}
				else {
					res.end(JSON.stringify({
						success: false,
						error: result.error
					}));
				}
			});
		});
	}
}

// handlers["/deliveries"] = (req, res) => {
// 	if(req.url && req.url.startsWith("/add")) {
// 		file.serveFile("/delivery_add.html", 200, {}, req, res);
// 	}
// 	else {
// 		file.serveFile("/delivery.html", 200, {}, req, res);
// 	}
// };

// handlers["/delivery"] = (req, res) => {
// 	if(req.method == "GET") {
// 		understandRemainingPath(res, req.url, "deliveries_view");
// 	}
// 	else if(req.method == "POST") {
// 		extractData(req, (data) => {
// 			let obj_data = JSON.parse(data);
// 			let insert_data = {
// 				supplier_id : obj_data.supplier_id,
// 				item_id : obj_data.item_id,
// 				quantity : obj_data.quantity,
// 				buying_price : obj_data.buying_price,
// 				delivery_date : obj_data.delivery_date
// 			};
// 			insertIntoDB("deliveries", insert_data, (result) => {
// 				if(result.result) {
// 					let sqlQuery = "UPDATE " + conn.escapeId("inventory") + " SET quantity = quantity + 1 WHERE ?? = ?";
// 					let queryString = mysql.format(sqlQuery + ";", ["item_id", obj_data.item_id]);
// 					conn.query(queryString, (err, results, fields) => {
// 						if(err) {
// 							console.log("DB query error: " + err);
// 							res.end(JSON.stringify({error: err}));
// 						}
// 						else {
// 							console.log("DB query results: ");
// 							console.log(results);
// 							res.end(JSON.stringify({success : "Successfully added to deliveries and updated inventory"}));
// 						}
// 					});
// 				else if(result.error) {
// 					res.end(JSON.stringify({error: result.error}));
// 				}
// 			});
// 		});
// 	}
// };

handlers["/items"] = (req, res) => {
	if(req.url && req.url.startsWith("/brand") ) {
		let query = qs.parse(req.url.replace("/brand?", ""));
		findLikeFromDB("items", query, ["brand"], (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(req.url && req.url.startsWith("/stock_num") ) {
		let query = qs.parse(req.url.replace("/stock_num?", ""));
		findLikeFromDB("items", query, ["stock_num"], (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(req.url && req.url.startsWith("/name") ) {
		let query = qs.parse(req.url.replace("/name?", ""));
		findLikeFromDB("items", query, ["name"], (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(req.url && req.url.startsWith("/description") ) {
		let query = qs.parse(req.url.replace("/description?", ""));
		findLikeFromDB("items", query, ["description"], (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else {
		understandRemainingPath(res, req.url, "items");
	}
};

// handlers["/items_base"] = (req, res) => {
// 	understandRemainingPath(res, req.url, "items_base");
// };

handlers["/suppliers"] = (req, res) => {
	if(req.method == "POST") {
		let form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files) {
			console.log(fields);
			insertIntoDB("suppliers", fields, (result) => {
				if(result.result) {
					res.end(JSON.stringify({
						success: true,
						message: "Supplier was successfully added."
					}));
				}
				else {
					res.end(JSON.stringify({
						success: false,
						error: result.error
					}));
				}
			});
		});
	}
	else if(req.method == "GET") {
		understandRemainingPath(res, req.url, "suppliers");
	}
};

handlers["/customers"] = (req, res) => {
	if(req.method == "POST") {
		let form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files) {
			insertIntoDB("customers", fields, (result) => {
				if(result.result) {
					res.end(JSON.stringify({
						success: true,
						message: "Customer was successfully added."
					}));
				}
				else {
					res.end(JSON.stringify({
						success: false,
						error: result.error
					}));
				}
			});
		});
	}
	else if(req.method == "GET") {
		understandRemainingPath(res, req.url, "customers");
	}
};

/**
 * Used to validate json
 * @params String key
 * @params String type = "required | string | number"
 */
let validateJson = (json, key, type) => {
	let message = "";
	let success = 1;
	if(type == "required") {
		if(!json.hasOwnProperty(key)) {
			message = key + " is required."
		}
	}
	return {message: message, success: success};
}

http.createServer((req, res) => {
	let url = req.url.match(/(\/[^\/\?]*)([\/\?].+)?/);
	console.log(req.url);
	let h = handlers[url[1]];
	if(h) {
		req.url = "";
		if(url[2] != null) {
			req.url = decodeURI(url[2]);
		}
		h(req, res);
	}
	else {
		file.serve(req, res, (err) => {
			res.writeHead(404, {'Content-Type': 'text/html'});
			res.end("Error 404: Page not found");
		});
	}
}).listen(8080);

console.log("Server running");

let extractData = (req, callback) => {
	let data = "";
	req.on('data', (chunk) => {
		console.log("Extracting data..");
		data += chunk;
	});
	req.on('end', () => {
		console.log("Extracted: " + data);
		callback(data);
	});
};

let findFromDB = (table, obj, column = ["*"], callback) => {
	let sqlQuery = "SELECT ?? FROM " + conn.escapeId(table);
	let inserts = [column];
	for(let i = 0; obj && i < Object.keys(obj).length; i++) {
		sqlQuery += (i == 0) ? " WHERE ?? = ?" : " AND ?? = ?";
		inserts.push(Object.keys(obj)[i]);
		inserts.push(Object.values(obj)[i]);
	}
	let queryString = mysql.format(sqlQuery + ";", inserts);
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results: ")
			console.log(results);
			callback({result: results});
		}
	});
};

let insertIntoDB = (table, data, callback) => {
	let sqlQuery = "INSERT INTO " + conn.escapeId(table) + " SET ?";
	conn.query(sqlQuery, data, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results:");
			console.log(results);
			callback({result: results});
		}
	});
};

let findLikeFromDB = (table, query, columns = ["*"], callback) => {
	let sqlQuery;
	let queryString;
	if(query["any"]) {
		sqlQuery = "SELECT * FROM " + conn.escapeId(table) + " WHERE supplier LIKE ? OR item LIKE ? OR brand LIKE ?";
		let values = [];
		for(let i = 0; i < 3; i++) {
			values.push("%" + Object.values(query)[0] + "%");
		}
		queryString = mysql.format(sqlQuery + ";", values);
	}
	else {
		sqlQuery = "SELECT ?? FROM " + conn.escapeId(table) + " WHERE ?? LIKE ?";
		let values = [columns];
		for(let i in Object.keys(query)) {
			if(i != 0) {
				sqlQuery += "AND ?? LIKE ?";
			}
			let key = Object.keys(query)[i];
			values.push(key);
			values.push("%" + query[key] + "%");
		}
		queryString = mysql.format(sqlQuery + ";", values);
	}
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error : err});
		}
		else {
			console.log("DB query results: ")
			console.log(results);
			callback({result : results});
		}
	});
};

let understandRemainingPath = (res, url, table) => {							//rename function something to do with path
	if(url && url.startsWith("?")) {											//with query
		let query = qs.parse(url.replace("?", ""));
		findFromDB(table, query, ["*"], (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(url && url.startsWith("/search")) {
		let query = qs.parse(url.replace("/search?", ""));
		findLikeFromDB(table, query, ["*"], (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(url && url.startsWith("/id")) {										//select by id
		getPath(url, (paths) => {
			if(paths.length == 2) {
				let obj = {};
				obj[paths[0]] = paths[1];
				findFromDB(table, obj, ["*"], (rslt) => {
					res.writeHead(200, { "Content-Type" : "application/json" });
					res.end(JSON.stringify(rslt.result[0]));
				});
			}
		});
	}
	else if(url && url.startsWith("/all")) {
		findFromDB(table, {}, ["*"], (rslt) => {										//select all
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else {
		res.writeHead(404, {'Content-Type': 'text/html'});
		res.end("Error 404: Page not found");
	}
};

/* let verifyQuery = (columns, table, callback) => {
	switch(table) {
		case "deliveries_view":
			let deliveries_columns = ["id", "supplier", "item", "brand", "description", "quantity", "buying_price", "delivery_date"];
			for (let c of columns) {
				if(!deliveries_columns.includes(c)){
					callback(false);
				}
			}
			break;
		case "inventory_view":
			let inventory_columns = ["id", "supplier", "item", "brand", "description", "quantity", "buying_price", "delivery_date"];
			break;

	}
	callback(true);
}; */

let getPath = (url, callback) => {
	let reg = /[^\/]+/g;
	let arr = [];
	while (res = reg.exec(url)) {
		arr.push(res[0]);
	}
	callback(arr);
};
